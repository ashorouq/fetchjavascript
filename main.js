//authentication is deleted and other files are deleted too (included only main.js, index.html). 
//you can add authentication and include this file in your project

const userNames = ['yyx990803', 'sindresorhus', 'feross'];

const tab = document.getElementById("table")

//loop through user
for (let i=0; i<userNames.length; i++) {

    //get name
    const getName = async (username) => {
        const url = await fetch(`https://api.github.com/users/${username}`
        );

        let response = await url.json();
        getAName = response.name;
        //console.log(getAName);
        return getAName
    }
    //call async function and consume returned values after promise fulfills
    getName(userNames[i]).then((value) => { var cell = tab.rows[i+1].cells;
    cell[0].innerHTML = value;});

    //get projects numbers
    const getProject = async (username) => {
        const url = await fetch(`https://api.github.com/users/${username}`);

        let response = await url.json();
        getProjectNum = response.public_repos;
        //console.log(getProjectNum);
        return getProjectNum
    }
    //call async function and consume returned values after promise fulfills
    getProject(userNames[i]).then((value) => {var cell= tab.rows[i+1].cells;
    cell[1].innerHTML = value;});

    //get star numbers
    const getStar = async ( username ) => {
        const url = await fetch(`https://api.github.com/users/${username}/starred?per_page=1`);

        let header = url.headers.get("link");
        //console.log(header)
        //split header
        const splitHeader = header.split(",").map(sp=> {
                return { "last": sp.split(";")[0] }
            }
        )

        let result =  splitHeader[1].last;
        //console.log(result)
        //regular expression
        getAStar = result.match("&page=(.*)>")[1]
        console.log(getAStar)
        //starOutput = getAStar;
        return getAStar
    }
    //call async function and consume returned values after promise fulfills
    getStar(userNames[i]).then((value) => {var cell= tab.rows[i+1].cells;
    cell[2].innerHTML = value;});
}